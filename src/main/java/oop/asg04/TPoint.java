package oop.asg04;

//TPoint.java
/*
 This is just a trivial "struct" type class --
 it simply holds an int x/y point for use by Tetris,
 and supports equals() and toString().
 We'll allow public access to x/y, so this
 is not an object really.
 
 Đây là lớp/struct đơn giản, chỉ lưu một cặp x,y dùng cho Tetris
 và hỗ trợ equals() và toString().
 Ta sẽ cho phép truy nhập public tới x/y.
 */
public class TPoint {
	public int x;
	public int y;

	// Creates a TPoint based in int x,y
	// Tạo một đối tượng TPoint dựa trên hai giá trị x và y
	public TPoint(int x, int y) {
		// questionable style but convenient --
		// params with same name as ivars
		this.x = x;
		this.y = y;
	}

	// Creates a TPoint, copied from an existing TPoint
	// Tạo Tpoint mới là bản sao của một TPoint có sẵn
	public TPoint(TPoint point) {
		this.x = point.x;
		this.y = point.y;
	}

	// Standard equals() override
	// Cài đè equals chuẩn để có thể so sanh TPoint
	public boolean equals(Object other) {
		// standard two checks for equals()
		if (this == other) return true;
		if (!(other instanceof TPoint)) return false;

		// check if other point same as us
		TPoint pt = (TPoint)other;
		return(x==pt.x && y==pt.y);
	}

	// Standard toString() override, produce
	// human-readable String from object
	// Cài đè toString để cho kết quả con người đọc được
	public String toString() {
		return "(" + x + "," + y + ")";
	}
}
