package oop.asg04;// Piece.java

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Piece {
	private TPoint[] body;
	private int[] skirt;
	private int width;
	private int height;
	private Piece next;

    private int max(int a, int b, int c, int d){
        if (a<b) a=b;
        if (c<d) c=d;
        if (a<c) a=c;
        return a;
    }

	static private Piece[] pieces;

	public Piece(TPoint[] points) {
        // YOUR CODE HERE
        int i,j,min;
        body = points;
        width = max(points[0].x, points[1].x, points[2].x, points[3].x) + 1;
        height = max(points[0].y, points[1].y, points[2].y, points[3].y) + 1;
        skirt = new int[width];
        for (i = 0; i < width;i++){
            min=4;
            for (j=0;j<4;j++)
                if (body[j].x==i)
                    if(body[j].y<min) min=body[j].y;
            skirt[i]=min;
        }
        next=null;
    }
	
	public Piece(String points) {
		this(parsePoints(points));
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public TPoint[] getBody() {
		return body;
	}

	public int[] getSkirt() {
		return skirt;
	}

	public Piece computeNextRotation() {
		int i;
        TPoint[] r = new TPoint[4];
        for (i=0;i<4;i++){
            r[i]=new TPoint(height-body[i].y-1,body[i].x);
        }
        Piece s = new Piece(r);

		return s; // YOUR CODE HERE
	}

	public Piece fastRotation() {
		return next;
	}
	
	public boolean equals(Object obj) {
		// standard equals() technique 1
		if (obj == this) return true;
		
		// standard equals() technique 2
		// (null will be false)
		if (!(obj instanceof Piece)) return false;
		Piece other = (Piece)obj;
		
		// YOUR CODE HERE
        int i,j,m=0;
        for (i=0;i<4;i++)
            for(j=0;j<4;j++)
                if (body[i].equals(other.body[j])) m++;
		if (m==4) return true;
        return false;
	}


	public static final String STICK_STR	= "0 0	0 1	 0 2  0 3";
	public static final String L1_STR		= "0 0	0 1	 0 2  1 0";
	public static final String L2_STR		= "0 0	1 0 1 1	 1 2";
	public static final String S1_STR		= "0 0	1 0	 1 1  2 1";
	public static final String S2_STR		= "0 1	1 1  1 0  2 0";
	public static final String SQUARE_STR	= "0 0  0 1  1 0  1 1";
	public static final String PYRAMID_STR	= "0 0  1 0  1 1  2 0";
	
	public static final int STICK = 0;
	public static final int L1	  = 1;
	public static final int L2	  = 2;
	public static final int S1	  = 3;
	public static final int S2	  = 4;
	public static final int SQUARE	= 5;
	public static final int PYRAMID = 6;
	
	public static Piece[] getPieces() {
		// lazy evaluation -- create static array if needed
		if (Piece.pieces==null) {
			// use makeFastRotations() to compute all the rotations for each piece
			Piece.pieces = new Piece[] {
				makeFastRotations(new Piece(STICK_STR)),
				makeFastRotations(new Piece(L1_STR)),
				makeFastRotations(new Piece(L2_STR)),
				makeFastRotations(new Piece(S1_STR)),
				makeFastRotations(new Piece(S2_STR)),
				makeFastRotations(new Piece(SQUARE_STR)),
				makeFastRotations(new Piece(PYRAMID_STR)),
			};
		}
		
		
		return Piece.pieces;
	}
	


	private static Piece makeFastRotations(Piece root) {
		// YOUR CODE HERE
        Piece nextRo = root.computeNextRotation();
        if (nextRo.equals(root)){ //SQUARE
            root.next=root;
            return root;
        } else root.next=nextRo;
        while (!nextRo.equals(root)){
            if (nextRo.computeNextRotation().equals(root)) {
                nextRo.next=root;
                return root;
            }else nextRo.next=nextRo.computeNextRotation();
            nextRo=nextRo.next;
        }
		return root; // YOUR CODE HERE
	}
	


	private static TPoint[] parsePoints(String string) {
		List<TPoint> points = new ArrayList<TPoint>();
		StringTokenizer tok = new StringTokenizer(string);
		try {
			while(tok.hasMoreTokens()) {
				int x = Integer.parseInt(tok.nextToken());
				int y = Integer.parseInt(tok.nextToken());
				
				points.add(new TPoint(x, y));
			}
		}
		catch (NumberFormatException e) {
			throw new RuntimeException("Could not parse x,y string:" + string);
		}
		
		// Make an array out of the collection
		TPoint[] array = points.toArray(new TPoint[0]);
		return array;
	}
}
