package oop.asg04;

import javax.swing.*;

public class JBrainTetris extends JTetris{
     private Brain mBrain = new DefaultBrain();
     private Brain.Move mMove = new Brain.Move();
     private int mCount ;
     
     protected JCheckBox brainMode ;
     JBrainTetris(int pixels){
         super(pixels);
         mCount = -1;
     }

     public JComponent createControlPanel(){
         JComponent panel = super.createControlPanel();
         panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	     panel.add(new JLabel("Brain:"));
	     brainMode = new JCheckBox("Brain active");
	     panel.add(brainMode);
         return panel;
}

    public void tick(int verb) {
        if(!brainMode.isSelected()) {}
        else{
            board.undo();
            if(verb == DOWN) {
                if(mCount != count) {
                    mMove = mBrain.bestMove(board, currentPiece, board.getHeight()-TOP_SPACE, mMove);
                    mCount = count;
                }
                if(mMove == null || mMove.piece == null || currentPiece == null) {
                    stopGame();
                    return;
                }

            }
            if(!currentPiece.equals(mMove.piece)) super.tick(ROTATE);
            if(currentX < mMove.x) super.tick(RIGHT);
            if(currentX > mMove.x) super.tick(LEFT);
        }
        super.tick(verb);
    }

    public static void main(String[] args) {
		// Set GUI Look And Feel Boilerplate.
		// Do this incantation at the start of main() to tell Swing
		// to use the GUI LookAndFeel of the native platform. It's ok
		// to ignore the exception.
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception ignored) { }
		
		JBrainTetris tetris = new JBrainTetris(20);
		JFrame frame = JBrainTetris.createFrame(tetris);
		frame.setVisible(true);
	}
}