package oop.asg04;// Board.java


public class Board	{
	private int width;
	private int height;
	private boolean[][] grid;
	private boolean DEBUG = true;
	boolean committed;
    private int[] widths;
    private int[] heights;
    private int MaxHeight;
    private boolean[][] grid2;
    private int[] widths2;
    private int[] heights2;
    private int MaxHeight2;

	public Board(int w, int h) {
        int i,j;
		width = w;
		height = h;
        widths = new int[height];
        heights= new int[width];
		grid = new boolean[width][height];
		committed = true;
        for (i = 0; i < width; i++) {
            for (j = 0; j < height; j++) {
                grid[i][j] = false;
            }
        }
        for (i = 0; i < height; i++) widths[i] = 0;
        for (i = 0; i < width; i++) heights[i] = 0;


        // YOUR CODE HERE
	}
	
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getMaxHeight() {
		return MaxHeight; // YOUR CODE HERE
	}
	
	public void sanityCheck() {
        int i,j,count=0;
		if (DEBUG) {
			// YOUR CODE HERE
            for (j = 0; j < height; j++) {
                count=0;
                for (i = 0; i < width; i++) {
                    if(grid[i][j] == true) count++;
                }
                if (widths[j] != count) throw new RuntimeException("False Widths");
            }
            for (i = 0; i < width; i++) {
                count = 0;
                for (j = 0; j < height; j++) {
                    if (grid[i][j]) {
                        count = j+1;
                    }
                }
                if(heights[i] != count) throw new RuntimeException("False Heights");
            }
            count=0;
            for (i = 0; i < width; i++) {
                if (heights[i] > count) count = heights[i];
            }
            if (count != MaxHeight) throw new RuntimeException("False Max height");
		}
	}
	
	public int dropHeight(Piece piece, int x) {
        int i,y=0;
        if (x<0||x>width) throw new RuntimeException("input ERROR");
        for (i=0;i<piece.getWidth();i++) {
            if (i + x>=width) throw new RuntimeException("OUT BOUND");
            else if (heights[i+x]-piece.getSkirt()[i]>y){
                y=heights[i+x]-piece.getSkirt()[i];
            }
        }
		return y; // YOUR CODE HERE
	}
	
	public int getColumnHeight(int x) {
		return heights[x]; // YOUR CODE HERE
	}
	
	public int getRowWidth(int y) {
		 return widths[y];  // YOUR CODE HERE
	}
	
	public boolean getGrid(int x, int y) {
        if (x > width || y > height) return true;
        return grid[x][y]; // YOUR CODE HERE
	}
	
	
	public static final int PLACE_OK = 0;
	public static final int PLACE_ROW_FILLED = 1;
	public static final int PLACE_OUT_BOUNDS = 2;
	public static final int PLACE_BAD = 3;
	
	public int place(Piece piece, int x, int y) {
		// flag !committed problem
		// ném ngoại lệ nếu place() được gọi 
		// khi bảng chưa được commit (committed == false)
		if (!committed) throw new RuntimeException("place commit problem");
		int result = PLACE_OK;
        committed = false;
        backup();
        int a,b,i;
        for (i=0;i<piece.getBody().length;i++) {
            a = x+piece.getBody()[i].x;
            b = y+piece.getBody()[i].y;
            if (x+piece.getWidth()>width||y+piece.getHeight()>height||x<0||y<0)
                return PLACE_OUT_BOUNDS;
            if (grid[a][b])
                return PLACE_BAD;
            grid[a][b] = true;
            widths[b]++;
            if(widths[b] == width) result = PLACE_ROW_FILLED;
            if(heights[a] < b+1) heights[a] = b+1;
            if(heights[a] > MaxHeight) MaxHeight = heights[a];
        }
        sanityCheck();
        return result;
	}
	

    private void shift(int y){
        for (int x = 0; x < width; x++) {
            for (int shift = y; shift < heights[x]; shift++) {
                grid[x][shift] = grid[x][shift+1];
            }
            grid[x][height-1] = false;
        }
    }

	public int clearRows() {
		int rowsCleared = 0;

        if(committed == true) backup();
        committed = false;
		// YOUR CODE HERE
        int x,y,i,j;
        for (y = 0; y < MaxHeight; y++) {
            if(widths[y] == width) {
                shift(y);
                rowsCleared++;
                for (i = 0; i < width; i++) {
                    x = 0;
                    for (j = 0; j < height; j++) {
                        if (grid[i][j]) {
                            x = j+1;
                        }
                    }
                    heights[i] = x;
                }
                for (j = y; j < height-1; j++) widths[j] = widths[j+1];
                widths[height-1] = 0;
                MaxHeight--;
                y-=1;
            }
        }
		sanityCheck();
		return rowsCleared;
	}



	public void undo() {
		// YOUR CODE HERE
        if (committed) {
            return;
        }
        int[] temp = widths;
        widths = widths2;
        widths2 = temp;

        temp = heights;
        heights = heights2;
        heights2 = temp;

        int temp2 = MaxHeight;
        MaxHeight = MaxHeight2;
        MaxHeight2 = temp2;

        boolean[][] temp3 = grid;
        grid = grid2;
        grid2 = temp3;

        committed = true;
	}
	
	
	public void commit() {
		committed = true;
	}

	public String toString() {
		StringBuilder buff = new StringBuilder();
		for (int y = height-1; y>=0; y--) {
			buff.append('|');
			for (int x=0; x<width; x++) {
				if (getGrid(x,y)) buff.append('+');
				else buff.append(' ');
			}
			buff.append("|\n");
		}
		for (int x=0; x<width+2; x++) buff.append('-');
		return(buff.toString());
	}

    private void backup() {
        int i;
        MaxHeight2 = MaxHeight;
        widths2=new int[height];
        heights2=new int[width];
        grid2=new boolean[width][height];
        System.arraycopy(widths, 0, widths2, 0, widths.length);
        System.arraycopy(heights, 0, heights2, 0, heights.length);
        for (i = 0; i < grid.length; i++) {
            System.arraycopy(grid[i], 0, grid2[i], 0, grid2[i].length);
        }
    }
}


