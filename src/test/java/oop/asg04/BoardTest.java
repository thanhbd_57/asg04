package oop.asg04;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BoardTest {
	Board b;
	Piece pyr1, pyr2, pyr3, pyr4, s, sRotated;
    Piece [] arrayOfGetPieces ;



    // This shows how to build things in setUp() to re-use
	// across tests.
	// Minh h?a cách thi?t l?p các test ?? dùng l?i

	// In this case, setUp() makes shapes,
	// and also a 3X6 board, with pyr placed at the bottom,
	// ready to be used by tests.
	// Trong tr??ng h?p này, setUp() t?o ra các hình,
	// và m?t b?ng 3x6, v?i pyramid ??t ? ?áy b?ng,
	// s?n sàng ?? dùng cho các test.
	@Before
	public void setUp() throws Exception {
		b = new Board(3, 6);

		pyr1 = new Piece(Piece.PYRAMID_STR);
		pyr2 = pyr1.computeNextRotation();
		pyr3 = pyr2.computeNextRotation();
		pyr4 = pyr3.computeNextRotation();

		s = new Piece(Piece.S1_STR);
		sRotated = s.computeNextRotation();

		b.place(pyr1, 0, 0);

        arrayOfGetPieces = Piece.getPieces () ;
	}

	// Check the basic width/height/max after the one placement
	// Ki?m tra chi?u r?ng/chi?u cao/max sau m?t l?n ??t m?nh
	@Test
	public void testSample1() {
		assertEquals(1, b.getColumnHeight(0));
		assertEquals(2, b.getColumnHeight(1));
		assertEquals(2, b.getMaxHeight());
		assertEquals(3, b.getRowWidth(0));
		assertEquals(1, b.getRowWidth(1));
		assertEquals(0, b.getRowWidth(2));
	}

	// Place sRotated into the board, then check some measures
	// ??t sRotated vào b?ng r?i ki?m tra các d? li?u liên quan
	@Test
	public void testSample2() {
		b.commit();
		int result = b.place(sRotated, 1, 1);
		assertEquals(Board.PLACE_OK, result);
		assertEquals(1, b.getColumnHeight(0));
		assertEquals(4, b.getColumnHeight(1));
		assertEquals(3, b.getColumnHeight(2));
		assertEquals(4, b.getMaxHeight());
	}

	// Make  more tests, by putting together longer series of
	// place, clearRows, undo, place ... checking a few col/row/max
	// numbers that the board looks right after the operations.

	// Hãy làm thêm test, s?p x?p các chu?i dài h?n c?a các l?nh
	// place, clearRows, undo, place... ki?m tra các s? li?u
	// col/row/max ngay sau các thao tác

    @Test
    public void testAllInSmallBoard (){
        b = new Board (5, 10) ;
        b.commit() ;
        assertEquals (Board.PLACE_OK, b.place(arrayOfGetPieces[Piece.L1],0,0)) ;
        assertEquals (3 ,b.getColumnHeight(0)) ;
        assertEquals (1, b.getColumnHeight(1)) ;
        assertEquals (0, b.getColumnHeight(2)) ;
        assertEquals (2, b.getRowWidth(0)) ;
        assertEquals (1, b.getRowWidth(2)) ;
        assertEquals (1, b.getRowWidth(1)) ;
        assertFalse (b.getGrid (2,0)) ;
        b.commit() ;
        assertEquals(Board.PLACE_OK, b.place(arrayOfGetPieces [Piece.L1], 2,0));
        b.commit();
        assertEquals ( Board.PLACE_ROW_FILLED, b . place ( arrayOfGetPieces [ Piece . PYRAMID].fastRotation() , 3 , 0 ) ) ;
        b.clearRows();
        b.commit();
        assertEquals ( 2 , b . getMaxHeight ( ) ) ;
        assertFalse ( b . getGrid ( 1 , 1 ) ) ;
        assertTrue ( b . getGrid ( 0 , 0 ) ) ;
        assertTrue ( b . getGrid ( 2 , 1 ) ) ;

    }


    @Test
    public void testDropHeight( ) {
        b = new Board ( 6 , 10 ) ;
        b . place ( arrayOfGetPieces [ Piece . L2 ] , 0 , 0 ) ;
        b . commit ( ) ;
        b . place ( arrayOfGetPieces [ Piece . S1 ] , 2 , 0 ) ;
        assertEquals ( 1 , b . dropHeight ( arrayOfGetPieces [ Piece . STICK ] , 0 ) ) ;
        assertEquals ( 3 , b . dropHeight ( arrayOfGetPieces [ Piece . STICK ] , 1 ) ) ;
        assertEquals ( 2 , b . dropHeight ( arrayOfGetPieces [ Piece . STICK ] , 3 ) ) ;
        assertEquals ( 0 , b . dropHeight ( arrayOfGetPieces [ Piece . STICK ] , 5 ) ) ;
        assertEquals ( 3 , b . dropHeight ( arrayOfGetPieces [ Piece . STICK ] . fastRotation ( ) , 0 ) ) ;
        assertEquals ( 3 , b . dropHeight ( arrayOfGetPieces [ Piece . STICK ] . fastRotation ( ) , 1 ) ) ;
        assertEquals ( 2 , b . dropHeight ( arrayOfGetPieces [ Piece . STICK ] . fastRotation ( ) , 2 ) ) ;
        assertEquals ( 3 , b . dropHeight ( arrayOfGetPieces [ Piece . L1 ] , 0 ) ) ;
        assertEquals ( 3 , b . dropHeight ( arrayOfGetPieces [ Piece . L1 ] , 1 ) ) ;
        assertEquals ( 2 , b . dropHeight ( arrayOfGetPieces [ Piece . L1 ] , 2 ) ) ;
        assertEquals ( 2 , b . dropHeight ( arrayOfGetPieces [ Piece . L1 ] , 4 ) ) ;
        assertEquals ( 3 , b . dropHeight ( arrayOfGetPieces [ Piece . L1 ] . fastRotation ( ) , 0 ) ) ;
        assertEquals ( 2 , b . dropHeight ( arrayOfGetPieces [ Piece . L1 ] . fastRotation ( ) , 2 ) ) ;
        assertEquals ( 3 , b . dropHeight ( arrayOfGetPieces [ Piece . L1 ] . fastRotation ( ) . fastRotation ( ) , 0 ) ) ;
        assertEquals ( 1 , b . dropHeight ( arrayOfGetPieces [ Piece . L1 ] . fastRotation ( ) . fastRotation ( ) , 1 ) ) ;
        assertEquals ( 0 , b . dropHeight ( arrayOfGetPieces [ Piece . L1 ] . fastRotation ( ) . fastRotation ( ) , 4 ) ) ;
        assertEquals ( 2 , b . dropHeight ( arrayOfGetPieces [ Piece . L1 ] . fastRotation ( ) . fastRotation ( ) . fastRotation ( ) , 0 ) ) ;
        assertEquals ( 1 , b . dropHeight ( arrayOfGetPieces [ Piece . L1 ] . fastRotation ( ) . fastRotation ( ) . fastRotation ( ) , 2 ) ) ;
        assertEquals ( 2 , b . dropHeight ( arrayOfGetPieces [ Piece . L1 ] . fastRotation ( ) . fastRotation ( ) . fastRotation ( ) , 3 ) ) ;

        assertEquals ( 2 , b . dropHeight ( arrayOfGetPieces [ Piece . L2 ] . fastRotation ( ) , 1 ) ) ;
        assertEquals ( 1 , b . dropHeight ( arrayOfGetPieces [ Piece . L2 ] . fastRotation ( ) , 3 ) ) ;
        assertEquals ( 1 , b . dropHeight ( arrayOfGetPieces [ Piece . L2 ] . fastRotation ( ) . fastRotation ( ) , 0 ) ) ;
        assertEquals ( 1 , b . dropHeight ( arrayOfGetPieces [ Piece . L2 ] . fastRotation ( ) . fastRotation ( ) , 2 ) ) ;
        assertEquals ( 2 , b . dropHeight ( arrayOfGetPieces [ Piece . L2 ] . fastRotation ( ) . fastRotation ( ) , 4 ) ) ;
    }

    @Test
    public void testPlace ( ) {
        b = new Board ( 6 , 10 ) ;
        assertEquals ( Board . PLACE_OK , b . place ( arrayOfGetPieces [ Piece . L2 ] , 0 , 0 ) ) ;
        assertEquals ( 1 , b . getColumnHeight ( 0 ) ) ;
        assertEquals ( 3 , b . getColumnHeight ( 1 ) ) ;
        assertEquals ( 0 , b . getColumnHeight ( 2 ) ) ;
        assertEquals ( 2 , b . getRowWidth ( 0 ) ) ;
        assertEquals ( 1 , b . getRowWidth ( 1 ) ) ;
        assertEquals ( 1 , b . getRowWidth ( 2 ) ) ;
        assertEquals ( 0 , b . getRowWidth ( 3 ) ) ;
        assertEquals ( 3 , b . getMaxHeight ( ) ) ;
        b . commit ( ) ;
        assertEquals ( Board . PLACE_OK , b . place ( arrayOfGetPieces [ Piece . S1 ] , 2 , 0 ) ) ;
        assertEquals ( 1 , b . getColumnHeight ( 2 ) ) ;
        assertEquals ( 2 , b . getColumnHeight ( 3 ) ) ;
        assertEquals ( 2 , b . getColumnHeight ( 4 ) ) ;
        assertEquals ( 0 , b . getColumnHeight ( 5 ) ) ;
        assertEquals ( 4 , b . getRowWidth ( 0 ) ) ;
        assertEquals ( 3 , b . getRowWidth ( 1 ) ) ;
        assertEquals ( 1 , b . getRowWidth ( 2 ) ) ;
        assertEquals ( 0 , b . getRowWidth ( 3 ) ) ;
        assertEquals ( 3 , b . getMaxHeight ( ) ) ;
        b . commit ( ) ;
        assertEquals ( Board . PLACE_OUT_BOUNDS , b . place ( arrayOfGetPieces [ Piece . S1 ] , 4 , 2 ) ) ;
        b . undo ( ) ;
        assertEquals ( Board . PLACE_OK , b . place ( arrayOfGetPieces [ Piece . S1 ] . fastRotation ( ) , 4 , 1 ) ) ;
        assertEquals ( 1 , b . getColumnHeight ( 0 ) ) ;
        assertEquals ( 3 , b . getColumnHeight ( 1 ) ) ;
        assertEquals ( 1 , b . getColumnHeight ( 2 ) ) ;
        assertEquals ( 2 , b . getColumnHeight ( 3 ) ) ;
        assertEquals ( 4 , b . getColumnHeight ( 4 ) ) ;
        assertEquals ( 3 , b . getColumnHeight ( 5 ) ) ;
        assertEquals ( 4 , b . getRowWidth ( 0 ) ) ;
        assertEquals ( 4 , b . getRowWidth ( 1 ) ) ;
        assertEquals ( 3 , b . getRowWidth ( 2 ) ) ;
        assertEquals ( 1 , b . getRowWidth ( 3 ) ) ;
        assertEquals ( 0 , b . getRowWidth ( 4 ) ) ;
        assertEquals ( 4 , b . getMaxHeight ( ) ) ;
        assertFalse ( b . getGrid ( 4 , 0 ) ) ;
        assertFalse ( b . getGrid ( 5 , 0 ) ) ;
        assertFalse ( b . getGrid ( 5 , 3 ) ) ;
        b . commit ( ) ;
        assertEquals ( Board . PLACE_BAD , b . place ( arrayOfGetPieces [ Piece . S1 ] . fastRotation ( ) , 1 , 1 ) ) ;
        b . undo ( ) ;
        assertEquals ( Board . PLACE_OK , b . place ( arrayOfGetPieces [ Piece . S2 ] , 1 , 2 ) ) ;
        assertEquals ( 1 , b . getColumnHeight ( 0 ) ) ;
        assertEquals ( 4 , b . getColumnHeight ( 1 ) ) ;
        assertEquals ( 4 , b . getColumnHeight ( 2 ) ) ;
        assertEquals ( 3 , b . getColumnHeight ( 3 ) ) ;
        assertEquals ( 4 , b . getColumnHeight ( 4 ) ) ;
        assertEquals ( 3 , b . getColumnHeight ( 5 ) ) ;
        assertEquals ( 4 , b . getRowWidth ( 0 ) ) ;
        assertEquals ( 4 , b . getRowWidth ( 1 ) ) ;
        assertEquals ( 5 , b . getRowWidth ( 2 ) ) ;
        assertEquals ( 3 , b . getRowWidth ( 3 ) ) ;
        assertEquals ( 0 , b . getRowWidth ( 4 ) ) ;
        assertEquals ( 4 , b . getMaxHeight ( ) ) ;
        assertFalse ( b . getGrid ( 2 , 1 ) ) ;
        assertFalse ( b . getGrid ( 3 , 3 ) ) ;
        b . commit ( ) ;
        assertEquals ( Board . PLACE_OUT_BOUNDS , b . place ( arrayOfGetPieces [ Piece . S2 ] . fastRotation ( ) , 5 , 3 ) ) ;
        b . undo ( ) ;
        assertEquals ( Board . PLACE_BAD , b . place ( arrayOfGetPieces [ Piece . SQUARE ] , 3 , 3 ) ) ;
        b . undo ( ) ;
        assertEquals ( Board . PLACE_OK , b . place ( arrayOfGetPieces [ Piece . SQUARE ] , 4 , 4 ) ) ;
        assertEquals ( 1 , b . getColumnHeight ( 0 ) ) ;
        assertEquals ( 4 , b . getColumnHeight ( 1 ) ) ;
        assertEquals ( 4 , b . getColumnHeight ( 2 ) ) ;
        assertEquals ( 3 , b . getColumnHeight ( 3 ) ) ;
        assertEquals ( 6 , b . getColumnHeight ( 4 ) ) ;
        assertEquals ( 6 , b . getColumnHeight ( 5 ) ) ;
        assertEquals ( 4 , b . getRowWidth ( 0 ) ) ;
        assertEquals ( 4 , b . getRowWidth ( 1 ) ) ;
        assertEquals ( 5 , b . getRowWidth ( 2 ) ) ;
        assertEquals ( 3 , b . getRowWidth ( 3 ) ) ;
        assertEquals ( 2 , b . getRowWidth ( 4 ) ) ;
        assertEquals ( 2 , b . getRowWidth ( 5 ) ) ;
        assertEquals ( 6 , b . getMaxHeight ( ) ) ;
        assertFalse ( b . getGrid ( 2 , 1 ) ) ;
        assertFalse ( b . getGrid ( 5 , 3 ) ) ;
        b . commit ( ) ;
        assertEquals ( Board . PLACE_ROW_FILLED , b . place ( arrayOfGetPieces [ Piece . STICK ] , 0 , 1 ) ) ;
        assertEquals ( 5 , b . getColumnHeight ( 0 ) ) ;
        assertEquals ( 4 , b . getColumnHeight ( 1 ) ) ;
        assertEquals ( 4 , b . getColumnHeight ( 2 ) ) ;
        assertEquals ( 3 , b . getColumnHeight ( 3 ) ) ;
        assertEquals ( 6 , b . getColumnHeight ( 4 ) ) ;
        assertEquals ( 6 , b . getColumnHeight ( 5 ) ) ;
        assertEquals ( 4 , b . getRowWidth ( 0 ) ) ;
        assertEquals ( 5 , b . getRowWidth ( 1 ) ) ;
        assertEquals ( 6 , b . getRowWidth ( 2 ) ) ;
        assertEquals ( 4 , b . getRowWidth ( 3 ) ) ;
        assertEquals ( 3 , b . getRowWidth ( 4 ) ) ;
        assertEquals ( 2 , b . getRowWidth ( 5 ) ) ;
        assertEquals ( 6 , b . getMaxHeight ( ) ) ;
        assertFalse ( b . getGrid ( 2 , 1 ) ) ;
        assertFalse ( b . getGrid ( 5 , 3 ) ) ;
        assertFalse ( b . getGrid ( 4 , 0 ) ) ;
        assertFalse ( b . getGrid ( 5 , 0 ) ) ;
    }
    @Test
    public void testPlace2 (){
        Piece[] pieces;
        b = new Board(5, 10);
        pieces = Piece.getPieces();
        b.place(pieces[Piece.L2], 3, 0);
        b.commit();
        // Check the basic width/height/max after the one placement
        assertEquals(0, b.getColumnHeight(0));
        assertEquals(0, b.getColumnHeight(1));
        assertEquals(0, b.getColumnHeight(2));
        assertEquals(1, b.getColumnHeight(3));
        assertEquals(3, b.getColumnHeight(4));
        assertEquals(2, b.getRowWidth(0));
        assertEquals(1, b.getRowWidth(1));
        assertEquals(1, b.getRowWidth(2));
        assertEquals(0, b.getRowWidth(3));
        assertEquals(3, b.getMaxHeight());

    }

}


